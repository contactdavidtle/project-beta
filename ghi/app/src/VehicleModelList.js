export default function VehicleModelList(props) {
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Manufacturer</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
      {props.vehicleModels.map(model => {
        return (
          <tr key={model.id}>
            <td>{model.name}</td>
            <td>{model.manufacturer.name}</td>
            <td><img height="100" width="auto"  className="img-max" src={model.picture_url} alt="car" /></td>
          </tr>
        )
      })}
      </tbody>
    </table>
  )
}
