export default function ServiceList(props){
  let inventoryVins = []
  props.automobiles.forEach(auto => inventoryVins.push(auto.vin))

  async function handleDelete (id) {
    const url = `http://localhost:8080/api/services/${id}`;
    const fetchConfig = {
      method: "delete",
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      props.getServices()

    }
  }

  function convertToDate(dateTimeObject) {
    return new Date(dateTimeObject)
  }

  async function handleComplete (id) {
    const url = `http://localhost:8080/api/services/${id}/complete`;
    const fetchConfig = {
      method: "PUT",
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      props.getServices()

    }
  }
  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Customer name</th>
          <th>Date</th>
          <th>Time</th>
          <th>Technician</th>
          <th>Reason</th>
          <th>{'\u2605'} = VIP</th>
        </tr>
      </thead>
      <tbody>
      {props.services.filter(service => service.completed === false).map(service => {
            return (
              <tr key={service.id}>
                <td>{service.vin}</td>
                <td className="justify-content-end">{(inventoryVins.includes(service.vin)) ? '\u2605' : null} {service.owner}</td>
                <td>{convertToDate(service.appointment_date_time).toDateString()}</td>
                <td>{convertToDate(service.appointment_date_time).toLocaleTimeString()}</td>
                <td>{service.technician.name}</td>
                <td>{service.reason}</td>
                <td>
                  <button onClick={() => handleDelete(service.id)} type="button" className="btn btn-outline-danger">Cancel</button>
                  <button onClick={() => handleComplete(service.id)} type="button" className="btn btn-outline-success">Complete</button>
                </td>
              </tr>
            );
          })}
      </tbody>
    </table>
  );
}
