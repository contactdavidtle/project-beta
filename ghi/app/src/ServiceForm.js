import React, {useState} from "react";

export default function ServiceForm(props) {
  const [vin, setVIN] = useState("")
  const [owner, setOwner] = useState("")
  const [apptDateTime, setApptDateTime] = useState("")
  const [reason, setReason] = useState("")
  const [technician, setTechnician] = useState("")

  const handleVinChange = event => {
    const value = event.target.value
    setVIN(value)
  }

  const handleOwnerNameChange = event => {
    const value = event.target.value
    setOwner(value)
  }

  const handleApptDateTimeChange = event => {
    const value = event.target.value
    setApptDateTime(value)
  }

  const handleReasonChange = event => {
    const value = event.target.value
    setReason(value)
  }

  const handleTechnicianChange = event => {
    const value = event.target.value
    setTechnician(value)
  }


  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.vin = vin
    data.owner = owner
    data.appointment_date_time = apptDateTime
    data.reason = reason
    data.technician = technician


    const url = 'http://localhost:8080/api/services';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(url, fetchConfig);
    if (response.ok) {
      setVIN("")
      setOwner("")
      setApptDateTime("")
      setReason("")
      setTechnician("")
      props.getServices()
    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Make a service appointment</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input onChange={handleVinChange} placeholder="VIN" required type="text" name="VIN" id="VIN" className="form-control" value={vin} />
              <label htmlFor="VIN">VIN</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleOwnerNameChange} placeholder="owner_name" required type="text" name="owner_name" id="owner_name" className="form-control" value={owner} />
              <label htmlFor="owner_name">Owner Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleApptDateTimeChange} placeholder="appt" required type="datetime-local" name="appt" id="appt" className="form-control" value={apptDateTime} />
              <label htmlFor="appt">Appointment Date and Time</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleReasonChange} placeholder="reason" required type="text" name="reason" id="reason" className="form-control" value={reason} />
              <label htmlFor="reason">Reason</label>
            </div>
            <div className="mb-3">
              <select onChange={handleTechnicianChange} required name="technicians" id="technicians" className="form-select" >
                <option value="">Assign a technician</option>
                {props.technicians.map(technician => {
                      return (
                          <option key={technician.id} value={technician.id}>
                              {technician.name} - {technician.employee_number}
                          </option>
                      )
                    })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}
