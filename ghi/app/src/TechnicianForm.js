import React, {useState} from "react";

export default function ServiceForm(props) {
  const [name, setName] = useState("")
  const [employeeNumber, setEmployeeNumber] = useState("")

  const handleNameChange = event => {
    const value = event.target.value
    setName(value)
  }

  const handleEmployeeNumber = event => {
    const value = event.target.value
    setEmployeeNumber(value)
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.name = name
    data.employee_number = employeeNumber

    const shoesURL = 'http://localhost:8080/api/technicians';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(shoesURL, fetchConfig);
    if (response.ok) {
      setName("")
      setEmployeeNumber("")
      props.getTechnicians()
    }
  }

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Enter a new technician</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} placeholder="name" required type="text" name="name" id="name" className="form-control" value={name} />
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleEmployeeNumber} placeholder="employee_number" required type="number" name="employee_number" id="employee_number" className="form-control" value={employeeNumber} />
              <label htmlFor="employee_number">Employee Number</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}
