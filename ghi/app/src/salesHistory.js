import { useState } from "react";

function SalesHistory(props) {
    
    const [salesHistory, setSalesHistory] = useState([]);
    
    const handleChange = async (event) => {
        event.preventDefault();
        const person_id = event.target.value;
        const salesHistoryUrl = `http://localhost:8090/api/salesemployees/${person_id}/salesrecords/`;
        const response = await fetch(salesHistoryUrl);
        if (response.ok) {
            const salesHistory = await response.json();
            setSalesHistory(salesHistory.sales_record);
            props.getSalesPerson()
        }
    };

    return (
        <div>
            <h1 className="mt-4 mb-4">Employee Sales History</h1>
            <select onChange={handleChange} id="sales-person-sales-history" name="Sales Person" className="form-select mb-3">
                <option value="">Choose a Sales Employee</option>
                {props.salesPeople.map((salesPerson) => {
                    return (
                        <option key={salesPerson.id} value={salesPerson.id}>
                            {salesPerson.name}
                        </option>
                    );
                })}
            </select>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Sales Employee</th>
                        <th>Customer</th>
                        <th>Automobile VIN</th>
                        <th>Sale Price</th>
                    </tr>
                </thead>
                <tbody >
                    {salesHistory.map((sale) => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.sales_person.name}</td>
                                <td>{sale.customer.name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>${sale.price.toLocaleString()}.00</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
export default SalesHistory;
