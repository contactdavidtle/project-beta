import React, {useEffect, useState} from "react";

function SalesRecordForm(props){
    const [automobiles, setAutomobiles] = useState([])
    const [automobile, setAutomobile] = useState('');
    const [salesPerson, setSalesPerson] = useState('');
    const [customer, setCostumer] = useState('');
    const [price, setPrice] =useState('');

    const handleAtimobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value)
    }

    const handleSalesPersonChange = (event) => {
        const value = event.target.value;
        setSalesPerson(value)
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCostumer(value)
    }

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value)
    }

    const handleSubmit = async (event) =>{
        event.preventDefault();
        const data = {};
        data.automobile = automobile
        data.sales_person = salesPerson
        data.customer = customer
        data.price = price
        const url = "http://localhost:8090/api/salesrecords/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
            'Content-Type': 'application/json',
            },
        }
        const response = await fetch (url, fetchConfig);
        if (response.ok){
            setAutomobile('');
            setSalesPerson('');
            setCostumer('');
            setPrice('');
            props.getSalesPerson();
            props.getCustomer();
            props.getListSales();
            fetchData();
        }
    }

    const fetchData = async () => {
        const url = "http://localhost:8090/api/automobiles/"
        const response = await fetch (url)
        if (response.ok){
            const data = await response.json();
            setAutomobiles(data.automobiles)
        }
    }

    useEffect (() => {
        fetchData();
    }, [])

    return (
        <div className="container">
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new sale</h1>
              <form onSubmit ={handleSubmit} id="create-new-sales-record-form">
              <div className="mb-3">
                <select onChange={handleAtimobileChange} required name="automobile" id="automobile" className="form-select" value={automobile}>
                  <option value="">Choose an automobile</option>
                  {automobiles.filter(auto => auto.has_sold === false).map(auto =>{
                    return (
                        <option key={auto.vin} value={auto.vin}>
                            {auto.vin}
                        </option>
                    )
                })}
                </select>
                </div>
                <div className="mb-3">
                <select onChange={handleSalesPersonChange} required name="sales_person" id="sales_person" className="form-select" value={salesPerson} >
                  <option value="">Choose a sales person</option>
                  {props.salesPeople.map(person =>{
                    return (
                        <option key={person.id} value={person.id}>
                            {person.name}
                        </option>
                    )
                  })}
                </select>
                </div>
                <div className="mb-3">
                <select onChange={handleCustomerChange} required name="customer" id="customer" className="form-select" value={customer}>
                  <option value="">Choose a customer</option>
                  {props.customers.map(customer =>{
                    return (
                        <option key={customer.id} value={customer.id}>
                            {customer.name}
                        </option>
                    )
                  })}
                </select>
                </div>
                <div className="form-floating mb-3">
                  <input onChange={handlePriceChange}placeholder="price" required type="number" name="price" id="price" className="form-control" value={price} />
                    <label htmlFor="price">Sale price </label>

                </div>
                <button className="btn btn-success btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
)

}
export default SalesRecordForm
