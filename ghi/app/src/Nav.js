import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" to="/">Home</NavLink>
            </li>
            <div className="dropdown">
              <button className="btn btn-success dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Inventory
              </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="inventory/manufacturers">Manufacturers</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="inventory/manufacturers/new">{'\u21B3'}New manufacturer</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="inventory/vehicle_models">Vehicle Models</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="inventory/vehicle_models/new">{'\u21B3'}New vehicle model</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="inventory/automobiles">Automobiles</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="inventory/automobiles/new">{'\u21B3'}New automobile</NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn btn-success dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Service
              </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="services/pending">Pending appointments</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="services/history">Service History</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="services/new">New appointment</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="technicians/new">New technician</NavLink>
                </li>
              </ul>
            </div>
            <div className="dropdown">
              <button className="btn btn-success dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </button>
              <ul className="dropdown-menu">
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="sales/records">Sales records</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="sales/history">Sales history</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="sales/new">New sale</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="/customers/new">New customer</NavLink>
                </li>
                <li className="nav-item">
                  <NavLink className="dropdown-item" to="/employees/new">New Employee</NavLink>
                </li>
              </ul>
            </div>
          </ul>

        </div>
      </div>
    </nav>
  )
}

export default Nav;
