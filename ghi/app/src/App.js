import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ServiceForm from './ServiceForm';
import EmployeeForm from "./EmployeeForm";
import TechnicianForm from './TechnicianForm';
import ServiceHistory from './ServiceHistory';
import ServiceList from './ServiceList';
import { useEffect, useState } from 'react';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobileList';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import VehicleModelForm from './VehicleModelForm';
import VehicleModelList from './VehicleModelList';

import CustomerForm from './CustomerForm';
import SalesRecordForm from './SalesRecordForm';
import ListSales from './ListSales';
import SalesHistory from './salesHistory';


function App() {
  const [technicians, setTechnicians] = useState([])
  const [services, setServices] = useState([])
  const [serviceHistory, setServiceHistory] = useState([])

  const [manufacturers, setManufacturers] = useState([])
  const [vehicleModels, setVehicleModels] = useState([])
  const [automobiles, setAutomobiles] = useState([])

  const [salesPeople, setSalesPeople] = useState([]);
  const [customers, setCustomers]= useState([]);
  const [listSales, setListSales] = useState([]);


  const getTechnicians = async () => {
    const url = "http://localhost:8080/api/technicians"
    const response = await fetch (url)
    if (response.ok){
        const data = await response.json();
        const technicians = data.technicians
        setTechnicians(technicians)
    }
  }

  const getServices = async () => {
    const url = "http://localhost:8080/api/services"
    const response = await fetch (url)
    if (response.ok){
        const data = await response.json();
        const services = data.services
        setServices(services)
    }
  }

  const getAutomobiles = async () => {
    const url = "http://localhost:8100/api/automobiles"
    const response = await fetch (url)
    if (response.ok){
        const data = await response.json();
        const automobiles = data.autos
        setAutomobiles(automobiles)
    }
  }

  const getVehicleModels = async () => {
    const url = "http://localhost:8100/api/models/"
    const response = await fetch (url)
    if (response.ok){
        const data = await response.json();
        const vehicleModels = data.models
        setVehicleModels(vehicleModels)
    }
  }

  const getManufacturers = async () => {
    const url = "http://localhost:8100/api/manufacturers/"
    const response = await fetch (url)
    if (response.ok){
        const data = await response.json();
        const manufacturers = data.manufacturers
        setManufacturers(manufacturers)
    }
  }

  const getSalesPerson = async()=> {
    const url = "http://localhost:8090/api/salesemployees/"
    const response = await fetch (url)
    if (response.ok){
        const data = await response.json()
        const salesPeople = data.salesPeople
        setSalesPeople(salesPeople)
    }
}

  const getCustomer = async()=> {
    const url = "http://localhost:8090/api/customers/"
    const response = await fetch (url)
    if (response.ok){
        const data = await response.json()
        const customers = data.customers
        setCustomers(customers)
    }
}

const getListSales = async () => {
  const url = "http://localhost:8090/api/salesrecords/";
  const response = await fetch(url);
  if (response.ok) {
      const data = await response.json();
      setListSales(data.sales_record)

  }
}

  useEffect(() => {
    getTechnicians();
    getServices();
    getAutomobiles();
    getSalesPerson();
    getCustomer();
    getListSales();
    getVehicleModels();
    getManufacturers();
  }, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="services" >
            <Route path="pending" element={<ServiceList services={services} automobiles={automobiles} getServices={getServices} />} />
            <Route path="new" element={<ServiceForm technicians={technicians} getServices={getServices} />}/>
            <Route path="history" element={<ServiceHistory services={services} serviceHistory={serviceHistory} setServiceHistory={setServiceHistory} />} />
          </Route>
          <Route path="technicians">
            <Route path="new" element={<TechnicianForm getTechnicians={getTechnicians}/>} />
          </Route>
          <Route path="sales">
            <Route path="records" element={<ListSales listSales={listSales} getListSales={getListSales}/>}/>
            <Route path="history" element={<SalesHistory salesPeople={salesPeople} getSalesPerson={getSalesPerson}  />}/>
            <Route path="new" element={<SalesRecordForm customers={customers} salesPeople={salesPeople} getSalesPerson={getSalesPerson} getCustomer={getCustomer} getListSales={getListSales}/>}/>
          </Route>
          <Route path="customers">
            <Route path="new" element={<CustomerForm getCustomer={getCustomer}/>}/>
          </Route>
          <Route path="employees">
            <Route path="new" element={<EmployeeForm getSalesPerson={getSalesPerson}/>}/>
          </Route>
          <Route path="inventory">
            <Route path="manufacturers">
              <Route index element={<ManufacturerList manufacturers={manufacturers} />} />
              <Route path="new" element={<ManufacturerForm getManufacturers={getManufacturers} />} />
            </Route>
            <Route path="vehicle_models">
              <Route index element={<VehicleModelList vehicleModels={vehicleModels} />} />
              <Route path="new" element={<VehicleModelForm manufacturers={manufacturers} getVehicleModels={getVehicleModels} />} />
            </Route>
            <Route path="automobiles">
              <Route index element={<AutomobileList automobiles={automobiles} />}/>
              <Route path="new" element={<AutomobileForm vehicleModels={vehicleModels} getAutomobiles={getAutomobiles} />} />
            </Route>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
