import React, {useState} from "react";

export default function AutomobileForm(props) {
  const [color, setColor] = useState("")
  const [year, setYear] = useState("")
  const [vin, setVIN] = useState("")
  const [vehicleModel, setVehicleModel] = useState("")

  const handleColorChange = event => {
    const value = event.target.value
    setColor(value)
  }

  const handleYearChange = event => {
    const value = event.target.value
    setYear(value)
  }

  const handleVINChange = event => {
    const value = event.target.value
    setVIN(value)
  }

  const handleVehicleModel = event => {
    const value = event.target.value
    setVehicleModel(value)
  }

  const handleSubmit = async (event) =>{
    event.preventDefault();

    const data = {};
    data.color = color
    data.year = year
    data.vin = vin
    data.model_id = vehicleModel
    const url = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(data),
        headers: {
        'Content-Type': 'application/json',
        },
    }

    const response = await fetch(url, fetchConfig);
    if(response.ok){
      setColor("")
      setYear("")
      setVIN("")
      setVehicleModel("")
      props.getAutomobiles()

    }

}


  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Enter a new automobile</h1>
          <form onSubmit={handleSubmit}>
            <div className="form-floating mb-3">
              <input onChange={handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" value={color} />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleYearChange} placeholder="year" required type="number" name="year" id="year" className="form-control" value={year} />
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleVINChange} minLength="17" maxLength="17" placeholder="VIN" required type="text" name="VIN" id="VIN" className="form-control" value={vin} />
              <label htmlFor="VIN">VIN</label>
            </div>
            <div className="mb-3">
              <select onChange={handleVehicleModel} required name="model" id="model" className="form-select" value={vehicleModel} >
                <option value="">Model</option>
                {props.vehicleModels.map(model => {
                      return (
                          <option key={model.id} value={model.id}>
                              {model.name}
                          </option>
                      )
                    })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}
