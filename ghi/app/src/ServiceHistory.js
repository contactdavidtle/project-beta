import { useState } from "react";

export default function ServiceHistory(props){
    const [vin, setVIN] = useState("")

    function convertToDate(dateTimeObject) {
        return new Date(dateTimeObject)
      }

    const handleVINChange = event => {
        const value = event.target.value
        setVIN(value)
    }

    function handleSearch(vin) {
        props.setServiceHistory(props.services.filter(service => service.vin === vin))
    }

    return (
        <>
        <div className="input-group">
            <input onChange={handleVINChange} type="search" className="form-control rounded" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
            <button onClick={ () => handleSearch(vin)} type="button" className="btn btn-success">Search VIN</button>
        </div>
        <table className="table table-striped">
        <thead>
            <tr>
            <th>VIN</th>
            <th>Customer name</th>
            <th>Date</th>
            <th>Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th></th>
            </tr>
        </thead>
        <tbody>
        {props.serviceHistory.filter(service => service.completed === true).map(service => {
            return (
              <tr key={service.id}>
                <td>{service.vin}</td>
                <td className="justify-content-end">{service.owner}</td>
                <td>{convertToDate(service.appointment_date_time).toDateString()}</td>
                <td>{convertToDate(service.appointment_date_time).toLocaleTimeString()}</td>
                <td>{service.technician.name}</td>
                <td>{service.reason}</td>
              </tr>
            );
          })}
        </tbody>
        </table>
        </>
    )
}
