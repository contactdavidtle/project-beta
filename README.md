
# CarCar

<details open="open">
<summary>Table of Contents</summary>

- [Team](#team)
- [About](#about)
- [Application Diagram](#application-diagram)
- [Getting Started](#getting-started)
  - [Step by step installation](#step-by-step-installation)
- [Service microservice](#service-microservice)
    - [Endpoints for Service API](#endpoints-for-Service-api)
    - [JSON body examples for Post Requests](#json-examples-for-requests)
- [Sales microservice](#sales-microservice)
    - [Endpoints for Sales API](#endpoints-for-sales-api)
    - [JSON body examples for Post Requests](#json-samples-for-requests)



</details>

---

## Team
<table>
<tr>
<td>
David Le - Service

Ana María Pedroza - Sales
</td>
</tr>
</table>

## About

<table>
<tr>
<td>

Welcome to the Inventory Car App! This app allows you to manage your car inventory, sales, and service all in one place.

The app has a front-end built in React and a microservice architecture. Each domain (inventory, sales, and service) is in a separate microservice, which allows for more efficient development and scalability.

The inventory domain allows you to add, update, and delete cars in your inventory. You can also view the details of individual cars, such as make, model, and year. The inventory microservice uses a poll/sub mechanism to provide real-time updates to the front-end, so you always have the most current information.

The sales domain allows you to track your car sales. You can add new sales, view sales record per person, add employee and add a potencial customer.

The service domain allows you add a technician,to schedule a appoiment, show apoiment list ans show service histoty.

</td>
</tr>
</table>


## Application Diagram

![diagram](diagram.png)

## Getting Started

### Step by step installation
 1. Select clone with HTTP, and copy the link
 2. Open your terminal and clone the repo-link
 ```sh
 git clone <repo-link>
```
3. Change your directry to the new repo
```sh
 git cd <name-rep>
```
4. Open project in visual studio code
```sh
 code .
```
5. Create a docker valume
```sh
 docker volume create beta-data
```
6. Build docker image
```sh
 docker-compose build
```
7. Run containers
```sh
 docker-compose up
```
8. Access website with http://localhost:3000/

<table>
<tr>
<td>

## Service microservice

The Service microservice integrates with the Inventory microservice by polling for data about automobiles in inventory, creating a copy of automobile information as value objects, allowing the Service API to handle its own automobile data without altering any data from the Inventory microservice. Users can create service appointments, create new technicians, view a list of all appointments, cancel or mark appointments as finished, and delete technicians if necessary.

Currently, value objects of automobiles is redundant but is used to showcase the implementation of the poller and as an alternative method of implementation. VIP status is currently implemented on the frontend by fetching automobile vin data directly from the inventory microservice.

### Endpoints for Service API

| Action                     | Method     | URL                                                                 |
| -------------------------- | ------------------ | --------------------------------------------------------------------------- |
| CREATE TECHNICIAN         | POST| http://localhost:8080/api/technicians    |
| CREATE SERVICE APPOINTMENT| POST|  http://localhost:8080/api/services    |
| LIST ALL TECHNICIANS      | GET| http://localhost:8080/api/technicians    |
| LIST ALL APPOINTMENTS     | GET | http://localhost:8080/api/services                         |
| VIEW A SERVICE APPOINTMENT| GET |  http://localhost:8080/api/services/:serviceID|
| FINISH A SERVICE APPOINTMENT| PUT| http://localhost:8080/api/services/:serviceID/complete|
| DELETE A SERVICE APPOINTMENT| DELETE| http://localhost:8080/api/services/:serviceID|


### JSON Examples for Requests

#### POST request to http://localhost:8080/api/technicians
```sh
 {
	"name": "Bob the technician",
	"employee_number": 23423434
}
```

#### POST request to http://localhost:8080/api/services
```sh
{
	"owner": "David Le",
	"appointment_date_time": "2023-01-24T01:08:34+00:00",
	"vin": "JH4DC2380RS000036",
	"reason": "Dead Battery",
	"technician": 1
}
```

### JSON return Examples for Get Requests

#### GET request to http://localhost:8080/api/technicians
returns
```sh
{
	"technicians": [
		{
			"name": "Bob the technician",
			"employee_number": 23423434,
			"id": 1
		}
	]
}
```

#### GET request to http://localhost:8080/api/services
returns
```sh
{
	"services": [
		{
			"href": "/api/services/1",
			"id": 1,
			"owner": "Me",
			"appointment_date_time": "2023-01-24T01:08:34+00:00",
			"vin": "JH4DB1641NS802336",
			"reason": "Dead Battery",
			"completed": false,
			"technician": {
				"name": "Bob the technician",
				"employee_number": 23423434,
				"id": 1
			}
		}
	]
}
```

#### GET REQUEST to http://localhost:8080/api/services/1
returns
```sh
{
	"href": "/api/services/1",
	"id": 1,
	"owner": "Me",
	"appointment_date_time": "2023-01-24T01:08:34+00:00",
	"vin": "JH4DB1641NS802336",
	"reason": "Dead Battery",
	"completed": false,
	"technician": {
		"name": "Bob the technician",
		"employee_number": 23423434,
		"id": 1
	}
}
```

### OTHER REQUESTS

#### PUT REQUEST to http://localhost:8080/api/services/1/complete
Sets service appointment property "completed = true"
No body required.


#### DELETE REQUEST to http://localhost:8080/api/services/1
No body required
returns:
```sh
{deleted: true}
```

</td>
</tr>
</table>

<table>
<tr>
<td>

##  Sales microservice

The Sales microservice integrates with the Inventory microservice by polling for data about automobiles in inventory and creating a copy of automobile information as value objects, allowing the Sales microservice to handle its own automobile data without altering any data from the Inventory microservice. Users can create new sales, view existing sales records per person, add employees, and add potential customers.

### Endpoints for Sales API

| Action                     | Method     | URL                                                                 |
| -------------------------- | ------------------ | --------------------------------------------------------------------------- |
|LIST EMPLOYEES              | GET| http://localhost:8090/api/salesemployees/|
|LIST SALES EMPLOYEE RECORD | GET | http://localhost:8090/api/salesemployees/int:Employee_id/salesrecords/|
| CRETE SALES EMPLOYEE     | POST| http://localhost:8090/api/salesemployees/|
| LIST POTENCIAL CUSTOMER  | GET |  http://localhost:8090/api/customers/|
| CREATE POTENCIAL CUSTOMER| POST| http://localhost:8090/api/customers/|
| LIST SALES RECORD        | GET |  http://localhost:8090/api/salesrecords/|
| CREATE SALES RECORD      | POST|  http://localhost:8090/api/salesrecords/|


### JSON samples for Requests

#### Create a Sales Employee

```sh
 {
	"name": "Sheila Prada",
	"employee_number": 1
}
```
#### Create a Customer

```sh
 {
    "name": "Ronald Ramirez",
	"address": "3860 Saddle Rd, South Lake Tahoe, CA 96150",
	"phone_number": 214567367
}
```

#### Create a Sales Record

```sh
 {
	"automobile":"234DFRGS",
 	"sales_person":1,
	"customer":1,
	"price": 12345

}
```
### JSON return Examples for Get Requests

#### List employee
returns
```sh
{
	"salesPeople": [
		{
			"id": 1,
			"name": "Danish Khan",
			"employee_number": 1
		},
		{
			"id": 2,
			"name": "Alexa Ramirez",
			"employee_number": 2
		},
	]
}
```
#### List sales employee record
returns
```sh
{
	"sales_record": [
		{
			"automobile": {
				"vin": "1C3CC5FB2AN120174",
				"import_href": "/api/automobiles/1C3CC5FB2AN120174/",
				"has_sold": true
			},
			"sales_person": {
				"id": 1,
				"name": "Danish Khan",
				"employee_number": 1
			},
			"customer": {
				"id": 1,
				"name": "Yohena Ramirez",
				"address": "Chukkar dr",
				"phone_number": "34567"
			},
			"price": 135,
			"id": 2
		},
		{
			"automobile": {
				"vin": "234DFRGS",
				"import_href": "/api/automobiles/234DFRGS/",
				"has_sold": true
			},
			"sales_person": {
				"id": 1,
				"name": "Danish Khan",
				"employee_number": 1
			},
			"customer": {
				"id": 4,
				"name": "Martina Gonzales",
				"address": "Keller Rd",
				"phone_number": "234567891"
			},
			"price": 1231,
			"id": 10
		}
	]
}
```
#### List potencial customers
returns
```sh
{
	"customers": [
		{
			"id": 1,
			"name": "Yohena Ramirez",
			"address": "Chukkar dr",
			"phone_number": "34567"
		},
		{
			"id": 4,
			"name": "Martina Gonzales",
			"address": "Keller Rd",
			"phone_number": "234567891"
		},
	]
}
```

#### List sales record
returns
```sh
{
	"sales_record": [
		{
			"automobile": {
				"vin": "1C3CC5FB2AN120174",
				"import_href": "/api/automobiles/1C3CC5FB2AN120174/",
				"has_sold": true
			},
			"sales_person": {
				"id": 1,
				"name": "Danish Khan",
				"employee_number": 1
			},
			"customer": {
				"id": 1,
				"name": "Yohena Ramirez",
				"address": "Chukkar dr",
				"phone_number": "34567"
			},
			"price": 135,
			"id": 2
		},
		{
			"automobile": {
				"vin": "234DFRGS",
				"import_href": "/api/automobiles/234DFRGS/",
				"has_sold": true
			},
			"sales_person": {
				"id": 2,
				"name": "Alexa Ramirez",
				"employee_number": 2
			},
			"customer": {
				"id": 2,
				"name": "Julio Ramirez",
				"address": "port dr",
				"phone_number": "34562345"
			},
			"price": 1345,
			"id": 8
		},
	]
}
```

</td>
</tr>
</table>
