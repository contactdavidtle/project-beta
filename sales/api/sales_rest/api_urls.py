from django.urls import path
from .views import api_list_sale_people, api_list_potencial_customer, api_list_SalesRecord, api_list_AutomobileVO

urlpatterns = [
    path("automobiles/", api_list_AutomobileVO, name="api_list_AutomobileVO"),
    path("salesemployees/", api_list_sale_people, name="api_list_sale_people"),
    path("customers/", api_list_potencial_customer, name="api_list_potencial_customer"),
    path("salesrecords/", api_list_SalesRecord, name="api_list_SalesRecord"),
    path("salesemployees/<int:employee_id>/salesrecords/", api_list_SalesRecord, name="person_sale_history"),
]
