
from .models import AutomobileVO, SalesPerson, SalesRecord, Customer
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "import_href",
        "has_sold",
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "name",
        "employee_number",
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "address",
        "phone_number", 
    ]


class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        "automobile",
        "sales_person",
        "customer",
        "price",
        "id",
    ]
    encoders={
        "automobile":AutomobileVOEncoder(),
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
    }
@require_http_methods(["GET"])
def api_list_AutomobileVO(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVOEncoder
        )


@require_http_methods(["GET", "POST"])
def api_list_sale_people(request):
    if request.method == "GET":
        sales_people = SalesPerson.objects.all()
        return JsonResponse(
            {"salesPeople":sales_people},
            encoder=SalesPersonEncoder
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Could no create a sales person"},
                status=400,
            )


@require_http_methods(["GET","POST"])
def api_list_potencial_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers":customers},
            encoder=CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"message": "Could no create a customer"},
                status=400,
            )


@require_http_methods(["GET","POST"])
def api_list_SalesRecord(request, employee_id=None):
    if request.method == "GET":
        if employee_id == None:
            sales_record = SalesRecord.objects.all()
        else:
            sales_record = SalesRecord.objects.filter(sales_person=employee_id)
        return JsonResponse(
            {"sales_record": sales_record},
            encoder=SalesRecordEncoder
            )
    else:
        try:
            content = json.loads(request.body)
            auto_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=auto_vin)
            content["automobile"] = automobile
            employee_id = content["sales_person"]
            sales_person = SalesPerson.objects.get(id=employee_id)
            content["sales_person"] = sales_person
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
            sales_record = SalesRecord.objects.create(**content)
            automobile.has_sold = True
            automobile.save()
            return JsonResponse(
                sales_record,
                encoder=SalesRecordEncoder,
                safe=False
            )
        except:
            return JsonResponse (
                {"message":"Could not create a sales record"},
                status=400,
            )


