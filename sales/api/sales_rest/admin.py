from django.contrib import admin
from .models import SalesPerson, SalesRecord, Customer

# Register your models here.
admin.site.register(SalesRecord)
admin.site.register(SalesPerson)
admin.site.register(Customer)
