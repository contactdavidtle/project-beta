import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# This poller has been implemented for the rubric to showcase my ability to implement a poller
# and to show an alternative design.
# The current service_rest microservice does not need a poller to work.

from service_rest.models import InventoryVIN


def get_vins():
    response = requests.get("http://inventory-api:8000/api/automobiles/")
    content = json.loads(response.content)
    for auto in content["autos"]:
        InventoryVIN.objects.update_or_create(
            import_href=auto["href"],
            defaults={
                "vin": auto["vin"],
            },
        )


def poll():
    while True:
        print('Service poller polling for data')
        try:
            get_vins()
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
