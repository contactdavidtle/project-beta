from django.db import models
from django.urls import reverse


class InventoryVIN(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)

    def __str__(self):
        return self.vin

    class Meta:
        verbose_name_plural = "inventory VINs"


class Technician(models.Model):
    name = models.CharField(max_length=50)
    employee_number = models.IntegerField(unique=True)

    def __str__(self):
        return f"{self.name} - {self.employee_number}"

    def get_api_url(self):
        return reverse("technician", kwargs={"id": self.id})


class Service(models.Model):
    owner = models.CharField(max_length=50)
    appointment_date_time = models.DateTimeField(auto_now=False, auto_now_add=False)
    reason = models.TextField()
    vin = models.CharField(max_length=17)
    completed = models.BooleanField(default=False)
    technician = models.ForeignKey(
        Technician,
        related_name="services",
        on_delete=models.CASCADE
        )

    def complete_service(self):
        self.completed = True
        self.save()

    def __str__(self):
        return f"{self.owner} - {self.vin} - {self.appointment_date_time}"

    def get_api_url(self):
        return reverse("service", kwargs={"id": self.id})
