from django.contrib import admin
from .models import Technician, Service, InventoryVIN


admin.site.register(Technician)
admin.site.register(Service)
admin.site.register(InventoryVIN)
