from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Service, Technician


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
        "id",
    ]


class ServiceEncoder(ModelEncoder):
    model = Service
    properties = [
        "id",
        "owner",
        "appointment_date_time",
        "vin",
        "reason",
        "completed",
        "technician",
    ]
    encoders = {"technician": TechnicianEncoder()}


@require_http_methods(["GET", "POST"])
def services(request):
    if request.method == "GET":
        services = Service.objects.all()
        return JsonResponse(
            {"services": services},
            encoder=ServiceEncoder
        )
    else:  # POST
        try:
            content = json.loads(request.body)

            id = content["technician"]
            technician = Technician.objects.get(id=id)
            content["technician"] = technician

            service = Service.objects.create(**content)
            return JsonResponse(
                service,
                encoder=ServiceEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the servvice"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "DELETE"])
def service(request, id):
    if request.method == "GET":
        try:
            service = Service.objects.get(id=id)
            return JsonResponse(
                service,
                encoder=ServiceEncoder,
                safe=False,
            )
        except Service.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    else:  # Delete
        try:
            count, _ = Service.objects.get(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Service.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["PUT"])
def complete_service(request, id):
    if request.method == "PUT":
        try:
            service = Service.objects.get(id=id)
            service.complete_service()
            return JsonResponse(
                service,
                encoder=ServiceEncoder,
                safe=False,
            )
        except Service.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})


@require_http_methods(["GET", "POST"])
def technicians(request):
    if request.method == "POST":
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response
    elif request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder,
        )
