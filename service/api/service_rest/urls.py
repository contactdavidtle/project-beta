from django.urls import path

from .views import services, service, technicians, complete_service
urlpatterns = [
    path("services", services, name="services"),
    path("services/<int:id>", service, name="service"),
    path("services/<int:id>/complete", complete_service, name="complete_service"),
    path("technicians", technicians, name="technicians"),
]
